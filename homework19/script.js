const developersSpeed = [8, 8, 8];
const backlog = [16, 20, 10];
const daysToDeadline = 10;
const workHoursPerDay = 8;
// создаем функцию, передаем туда параметры daysToDeadline, workHoursPerDay, developersSpeed, backlog
function Deadline(daysToDeadline, workHoursPerDay, developersSpeed, backlog) {
    // считаем количевство старпоинтов
    const totalPoints = backlog.reduce((acc, points) => acc + points, 0);
    //считаем колличевство рабочих часов которые нам понадобяться и округляем до ближайшего целого 
    const totalWorkDays = Math.ceil(totalPoints / (developersSpeed.reduce((acc, speed) => acc + speed, 0) / workHoursPerDay)); // ceil - округление для ближайшего цеьелого 
    // создаем дату которая хранит в себе дату дедлайна?
    const currentDate = new Date(); 
    const deadline = new Date(currentDate.getTime() + (daysToDeadline * 24 * 60 * 60 * 1000));
    // если нам нужно меньше часов чем часов до дедлайна - победа
    if (totalWorkDays <= daysToDeadline) {
      const remainingDays = daysToDeadline - totalWorkDays;
      return `Усі завдання будуть успішно виконані за ${remainingDays} днів до настання дедлайну!`;
    // если нет - раздолбаи 
    } else {
      const extraHours = (totalWorkDays - daysToDeadline) * workHoursPerDay;
      return `Команді розробників доведеться витратити додатково ${extraHours} годин після дедлайну, щоб виконати всі завдання в беклозі`;
    }
  }
//вывод
const result = Deadline(daysToDeadline, workHoursPerDay, developersSpeed, backlog);
console.log(result);