const drawCircleButton = document.getElementById('drawCircleButton');
const circleInput = document.getElementById('circleInput'); 
const diameterInput = document.getElementById('diameter'); 
const drawButton = document.getElementById('drawButton'); 
const circlesContainer = document.getElementById('circlesContainer'); 

let circleCount = 0; // каунтер для счета кругов  

// оброботчик для кнопки 
drawCircleButton.addEventListener('click', () => {
  circleInput.style.display = 'block'; // при нажатии выводим блок диаметра кола 
});

// Оброботчик рисовалка
drawButton.addEventListener('click', () => {
  const diameter = parseInt(diameterInput.value, 10); // получаем диаметр и переделаем в число если какого-то чуда нам оно пришло как стринг 

  if (!isNaN(diameter)) { // проверка введино ли корректное число диаметра 
    const circle = document.createElement('div');
    circle.className = 'circle'; 
    circle.style.width = `${diameter}px`;
    circle.style.height = `${diameter}px`; 
    circle.style.backgroundColor = getRandomColor(); // вызываем функцию которая вставляет рандомный цвет 

    // оброботчик удаления кружочка 
    circle.addEventListener('click', () => {
      circlesContainer.removeChild(circle); 
      circleCount--; 
      repositionCircles(); //пересортировывем, вызывая функцию ниже 
    });

    circlesContainer.appendChild(circle); // добавляем круг в контейнер 
    circleCount++; // Увеличиваем каунтер

    if (circleCount >= 100) { 
      circleInput.style.display = 'none';
    }
  }
});

//рандомайзер цветов круга 
function getRandomColor() {
  const letters = '0123456789ABCDEF'; // как я понимаю именно это нам помогает выбрать рандомный цвет 
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

// пересортировка кругов 
function repositionCircles() {
  const circles = document.getElementsByClassName('circle');
  for (let i = 0; i < circles.length; i++) {
    circles[i].style.left = `${i % 10 * 60}px`; // Розташовуємо кола в рядки (10 кол в рядку)
    circles[i].style.top = `${Math.floor(i / 10) * 60}px`; // Розташовуємо кола в стовпці (10 кол в стовпці)
  }
}