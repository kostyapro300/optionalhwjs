const drawCircleButton = document.getElementById('drawCircleButton');
const circleInput = document.getElementById('circleInput');
const diameterInput = document.getElementById('diameter');
const drawButton = document.getElementById('drawButton');
const circlesContainer = document.getElementById('circlesContainer');

let circleCount = 0;

drawCircleButton.addEventListener('click', () => {
  circleInput.style.display = 'block';
});

drawButton.addEventListener('click', () => {
    const diameter = parseInt(diameterInput.value, 10);
    if (!isNaN(diameter)) {
      for (let i = 0; i < 100; i++) {
        const circle = document.createElement('div');
        circle.className = 'circle';
        circle.style.width = `${diameter}px`;
        circle.style.height = `${diameter}px`;
        circle.style.backgroundColor = getRandomColor();
  
        const row = document.querySelector('.row:last-child');
        if (!row || row.children.length === 10) {
          const newRow = document.createElement('div');
          newRow.className = 'row';
          circlesContainer.appendChild(newRow);
        }
  
        const newRow = document.querySelector('.row:last-child');
        newRow.appendChild(circle);
      }
  
      circleInput.style.display = 'none';
    }
  });
  
  circlesContainer.addEventListener('click', (event) => {
    const target = event.target;
    if (target.classList.contains('circle')) {
      target.remove();
      circleCount--;
      repositionCircles();
    }
  });
function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function repositionCircles() {
  const circles = document.getElementsByClassName('circle');
  for (let i = 0; i < circles.length; i++) {
    circles[i].style.left = `${i % 10 * diameter}px`;
    circles[i].style.top = `${Math.floor(i / 10) * diameter}px`;
  }
}