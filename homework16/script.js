function fibonacciRecursive(n) {
    if (n === 0) {
      return 0;
    } else if (n === 1 || n === -1) {
      return 1;
    } else if (n > 1) {
      return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2);
    } else { 
      return fibonacciRecursive(n + 2) - fibonacciRecursive(n + 1);
    }
  }
  // Пример использования:
  const n = parseInt(prompt('введите число'));
  const result = fibonacciRecursive(n);
  console.log(`Число Фибоначчи для n=${n} равно ${result}`);