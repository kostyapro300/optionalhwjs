let student = {
    name: "",
    surname: "",
    subjects: {},
}

student.name = prompt('Введіть ім\'я студента:');
student.surname = prompt('Введіть прізвище студента:');

while (true) {
    const subjects = prompt('Предмет')
    if (subjects === null) {
        break;
    }
    const grade = parseInt(prompt('Оценка'))
    if (!isNaN(grade)) {
        student.subjects[subjects] = grade 

    } else('Некоректна оценка. Спробуй ще раз')
}


// счет колличевства плохих оценочек 

let poorGrageCounter = 0;
for (const subjects in student.subjects) {
    if (student.subjects.hasOwnProperty(subjects)) {
        if(student.subjects[subjects] <4) {
            poorGrageCounter++
        }
    }
}

console.log('Ім\'я студента:', student.name);
console.log('Прізвище студента:', student.surname);
console.log('Оцінки студента:', student.subjects);

if(poorGrageCounter === 0) {
    console.log('Победа, переводим на след курс');
}

const gradesum = Object.values(student.subjects).reduce((sum,grade) => sum + grade,0)
const avarageGrade = gradesum / Object.values(student.subjects).length
console.log('Середній бал з предметів:', avarageGrade);

if (avarageGrade > 7) {
  console.log('Студенту призначено стипендію.');
}