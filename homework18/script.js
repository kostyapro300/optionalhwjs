function deepClone(obj) {
    if (obj === null || typeof obj !== 'object') {
      return obj; // Если это не объект или null, вернуть его как есть.
    }
  
    if (Array.isArray(obj)) {
      const newArray = [];
      for (let i = 0; i < obj.length; i++) {
        newArray[i] = deepClone(obj[i]); // Рекурсивно клонировать каждый элемент массива.
      }
      return newArray;
    }
  
    const newObj = {};
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        newObj[key] = deepClone(obj[key]); // Рекурсивно клонировать каждое свойство объекта.
      }
    }
  
    return newObj;
  }
  
  // например нам надо скопировать это 
  const originalObject = {
    name: 'kurosaki',
    age: 17,
    weapon: 'sword',
    address: {
      street: 'ylisha pyshkina',
      city: 'OAOaf',
    },
  };
  
  const clonedObject = deepClone(originalObject);
  console.log(originalObject);
  console.log(clonedObject);